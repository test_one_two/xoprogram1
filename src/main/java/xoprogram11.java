
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author User
 */
public class xoprogram11 {
    static int input;
    static char winner = '-';
    static boolean isFinish = false;
    static int row, col;
    static Scanner kb = new Scanner(System.in);
    static char[][] board = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'}};

    static char player = 'X';

    static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    static void showTable() {
        System.out.println("  1 2 3");
        for (int row = 0; row < board.length; row++) {
            System.out.print(row + 1 + " ");
            for (int col = 0; col < board.length; col++) {
                System.out.print(board[row][col] + " ");
            }
            System.out.println("");
        }

    }

    static void showTurn() {
        System.out.println(player + " turn");
    }

    static void input() {

        while (true) {

            System.out.println("Please input Row Col: ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            input++;
            //System.out.println("row: "+ row +"col: "+col);
            if (board[row][col] == '-') {
                board[row][col] = player;
                break;

            }
            System.out.println("Error");
        }

    }

    static void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (board[row][col] != player) {
                return;
            }
        }
        showTable();
        isFinish = true;
        winner = player;
    }

    static void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (board[row][col] != player) {
                return;
            }
        }
        showTable();
        isFinish = true;
        winner = player;
    }

    static void checkx() {
        if (board[0][0] == player && board[1][1] == player && board[2][2] == player) {
            showTable();
            isFinish = true;
            winner = player;
        } else if (board[2][0] == player && board[1][1] == player && board[0][2] == player) {
            showTable();
            isFinish = true;
            winner = player;
        }

    }

    static void checkdraw() {
        if(input == 8){
            showTable();
            isFinish = true;
            winner = '-';
        }
    }

    static void checkwin() {
        checkRow();
        checkCol();
        checkx();
        checkdraw();
    }

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    static void showResult() {
        if (winner == '-') {
            System.out.println("Draw!!!!!");
        } else {
            System.out.println(winner + " winner");
        }
    }

    static void showBye() {
        System.out.println("Bye bye .....");
    }

    public static void main(String[] args) {
        showWelcome();
        do {
            showTable();
            showTurn();
            input();
            checkwin();
            switchPlayer();
        } while (!isFinish);
        showResult();
        showBye();

    }
}
